using Trogon.PublicTransport.ViewModels;

using Windows.UI.Xaml.Controls;

namespace Trogon.PublicTransport.Views
{
    public sealed partial class MainPage : Page
    {
        public MainViewModel ViewModel { get; } = new MainViewModel();
        public MainPage()
        {
            InitializeComponent();
        }
    }
}
